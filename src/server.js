const express = require("express");
const routes = require("./routes");
const mongoose = require("mongoose");

const app = express();

mongoose.connect(
  "mongodb+srv://omnistack:omnistack@omnistack-tmbzd.mongodb.net/semana09?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

// req.query => query params
// req.params => /users/:params
// req.body => body.

// app.get("/", (req, res) => {
//   return res.json({ message: "Brenda minha vida 2" });
// });

// app.get('/users', (req, res) => {
//     return res.json({ message: req.query.idade });
// });

// app.put('/users/:id', (req, res) => {
//     return res.json({ message: req.params.id });
// })

app.use(express.json());
app.use(routes);

app.listen(3333);
